\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}
\usepackage{amssymb}
\usepackage{listings}
\setcounter{secnumdepth}{5}
\setcounter{tocdepth}{5}

\usepackage[english]{babel}
\usepackage{multirow}
\usepackage{url}

\title{Compiler Construction}
\author{Eric McNabb, 910723-2275,  eric.mcnabb@gmail.com \\
Sebastian Eriksson, 890604-4816, seberik@student.chalmers.se}

\begin{document}
\maketitle

\section{Usage}
In order to compile a Javalette file the following command will be executed \texttt{./jlc [file\_path]}. This compiles and links the Javalette program with lib/runtime.ll which will generate the object file a.out.

\section{Files structure}
\begin{verbatim}
/lib/				-- Runtime libraries
/doc/				-- Documentation
/src/				-- Compiler Source Code
/src/Javalette.cf 	-- Javalette language specification
/src/jlc.hs			-- Compiler
/src/TypeChecker.hs -- Typecker
/src/CodeGen.hs		-- LLVM Code Generator
/src/LLVM.hs			-- LLVM Helper Module
jlc					-- Compiler binary
\end{verbatim}

\section{Javalette Language}
The Javalette language is basically a light version of Java. The language is fully specified in the BNFC file Javalette.cf.

\subsection{Shift-reduce conflict}
The Javalette BNFC has one shift-reduce conflict arising from the definitions of if-statements with the optional else statement. The LR-parser could reduce before it reaches the optional else, but it could also shift the else token instead. The LR-parser we are using seems to behave in the way that it will try to find the longest match and thus this shift-reduce problem is harmless.

\section{Compiler}
The compiler compiles Javalette programs by first running the lexer and parser to obtain the AST. The type checker parses the AST and wraps expressions with type information. The code generator then gets the modified AST which it uses to generate LLVM code. After the code generator have generated LLVM code, the compiler will then assemble and link with lib/runtime. It then compiles the byte-code into assembly and lastly runs gcc to create the object file (a.out).

\subsection{Type checker} 
The type checker takes an AST and verifies that the types are correct. Additionally, if the types are correct it returns a modified version of the AST using \texttt{EType Type Expr} to wrap expressions with type information, for usage in the Code Generator. 

\subsubsection{Environment}
The environment in used in the type checker is passed by value rather than using the state monad, as is done in the code generator. The environment contains the function signatures (envSig), variables (envCxt), the current functions return type (defType) and the found return type.
\begin{verbatim}
data Env = Env
  { envSig :: Sig
  , envCxt :: Cxt
  , defType :: Type
  , retType :: Type
  }
\end{verbatim}
  
\subsubsection{Checking return type}
The type checker, will not only look for return statements in a function outside of if-else statements. It is also allowing return statements to be inside of if-else statements, if the if-else condition always guarantees a return value. It will this look for return statements within the first block if the if/if-else condition is an ELitTrue value. It will also look for return statements within the second (else) block if the condition in an if-else always is an ELitFalse.

\subsubsection{Expression types}
The type checker uses inferExp to infer types for expressions. The program tree is then modified by wrapping all of the expressions with a type (\texttt{EType <type> <expression>}). The types can thus be obtained easily by the code generator.

\subsection{Code Generator}
The code generator generates LLVM code \footnote{\url{http://llvm.org/docs/LangRef.html}} from the AST, that has been modified by the type checker to provide type information for expressions. 
\\
The main function compileProgram calls compileDefs which in turn calls compileStm which calls compileExpr. Thus it is compiling (generating code for) each function definition, its statements and their expressions.

\subsubsection{Enviroment}
The environment is handled by the state monad and contains all global information that is needed for the code generator. The contexts contains a list of variable contexts, which is all the blocks and variables in those blocks that are available to the current instruction. The code variable contains the code that is generated, numlabels and nextTmp simply makes sure that the temporary variables and labels get a unique index when created. The strings variable contains all strings in the program which have to be declared as global constants, outside of the function scope and then referenced within the function scope. The varcount map, keeps track of variable declarations, when trying to declare new instances of a already used variable name but in a new block. The heap variable is no longer used and should be removed, as array allocation is no longer done by trying to infer a size from the program tree but rather as specified in 4.3.1.
\begin{verbatim}
-- | Environment (handled by state monad)
data Env = Env
  { contexts      :: Contexts           -- Variables
  , code          :: [LLVM.Instruction] -- The generated code
  , numLabels     :: Int                -- Number of labels
  , nextTmp       :: Int                -- Number of temp vars
  , strings       :: [LLVM.Instruction] -- Keep track of global string constants
  , varCount      :: Map Ident Int      -- Maintain count of variables with same name
  , heap          :: Map Ident Int      -- Set the allocated space for arrays
  }

type Contexts = [VarContext]

-- | Variables in scope (symbol table)
data VarContext = VarContext
  { vars  :: Map Ident LLVM.Address     -- Variable address
  , vtype :: Map Ident Type             -- Set type of variable
  , ptype :: Map Ident Bool             -- Set variable type to
                                        -- pointer (true) or value (false)
  , next  :: LLVM.Address               -- Next free address pointer
  , isArg :: Map Ident Bool             -- Indicates that the variable is an argument
  }
\end{verbatim}

\subsection{Extensions}
The extensions that are implemented in the compiler are 1-dimensional arrays and for-loops. In the following sections the implementation details are discussed.

\subsubsection{Arrays}
\label{sec:arrays}
Arrays are implemented as structs where the first element is the array size and the second element is a pointer to the array elements \texttt{(type \{t, t* \})}.

The C-function calloc is used to allocate memory for the arrays. Calloc is given the number of elements and the size of each element. It then allocates, and zero-initializes the memory space. The return value of calloc is the address to the beginning of the memory space and this address is used to set the address of the array elements when declaring an array. The following llvm-code demonstrates this for integer (i32) arrays.

\begin{verbatim}
; Allocate space for the array struct
%a = alloca %i32array

; Get a pointer to the first element in the array struct
; i.e. the size and overwrite the size of the array
%t0 = getelementptr inbounds %i32array* %a, i32 0, i32 0
store i32 10, i32* %t0, align 4

; Allocate space for the array elements
%t1 = getelementptr i32* null, i32 1
%t2 = ptrtoint i32* %t1 to i32
%t3 = call i8* @calloc(i32 10, i32 %t2)
%t4 = bitcast i8* %t3 to i32*
%t5 = getelementptr %i32array* %a, i32 0, i32 1
store i32* %t4, i32** %t5
\end{verbatim}

\subsubsection{For-loops}
For-loops are implemented using getelementpointer and the arrays size to iterate over each element in the array. A loop-iterator variable is initialized to zero and then increased for each iteration, checking that it is smaller than the size of the array. As previously stated the size of the array is given by the first element in the array-struct.

\end{document}







