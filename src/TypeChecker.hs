module TypeChecker where

import Javalette.Abs
import Javalette.Print
import Javalette.ErrM

import Control.Monad

import Data.Functor
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Maybe

-- | Environment
data Env = Env
  { envSig :: Sig
  , envCxt :: Cxt
  , defType :: Type
  , retType :: Type
  }

-- | Function signature
type Sig = Map Ident FunType

-- | Context of local variables
type Cxt = [TypeChecker.Block]

type Block = Map Ident Type

-- | Function type
data FunType = FunType Type [Type]

typecheck :: Program -> Err (Program)
typecheck (Program defs) = do
  let env0 = Env
       { envSig = Map.fromList [
          (Ident "printInt", FunType Void [Int]),
          (Ident "printString", FunType Void [String]),
          (Ident "printDouble", FunType Void [Doub]),
          (Ident "readInt", FunType Int []),
          (Ident "readDouble", FunType Doub [])
        ]
       , envCxt = []
       , defType = Void
       , retType = Void
       }
  
  env <- foldM extendSig env0 defs

  FunType t ts <- lookupDef env (Ident "main")
  
  unless( t == Int ) $ fail $
          "(typecheck) expected main type int but found " ++ printTree t

  topDefs <- checkDefs env defs

  return $ Program topDefs

checkDefs :: Env -> [TopDef] -> Err [TopDef]
checkDefs env [] = return []
checkDefs env (d:ds) = do
  (def, env') <- checkDef env d
  def' <- checkDefs env' ds
  return $ [def] ++ def'

checkDef :: Env -> TopDef -> Err (TopDef, Env)
checkDef env (FnDef t f args (Block ss)) = do
  let env' = foldl (\ env (Arg t x) -> extendCxt env x t) (newBlock env) args
  env'' <- setDefType env' t
  (stmts, envp@Env{ retType = rt }) <- checkStms env'' ss

  unless (t == rt) $ fail $
        "(checkDef) expected " ++ printTree t ++ "("++ printTree f ++")" ++" type, but found " ++ printTree rt

  let topDef = (FnDef t f args (Block stmts))
  return (topDef, envp)

checkStms :: Env -> [Stmt] -> Err ([Stmt], Env)
checkStms env [] = return ([], env)
checkStms env (s:ss) = do
  (stmt, env') <- checkStm env s
  (stmt', env'') <- checkStms env' ss
  return ([stmt] ++ stmt', env'')

checkStm :: Env -> Stmt -> Err (Stmt, Env)
checkStm env@Env{defType = dt} s = case s of

  BStmt (Block ss) -> do
    (stmts, env'@Env{retType = r}) <- checkStms (newBlock env) ss

    env <- setRetType env r

    return $ (BStmt (Block stmts), env)

  While e s -> do
    (expr, t) <- inferExp env e
    unless (t `elem` [Bool]) $ fail $
          "(While) expected bool type, but found " ++ printTree t ++
          " when checking " ++ printTree s

    (stmt, env'@Env{retType = r}) <- checkStm env s

    case e of 
      ELitTrue -> do
          env <- setRetType env r
          return (While expr stmt, env)
      _ -> do
          return (While expr stmt, env)

  Decl t xs -> do
    (env',xs') <- updateVars env t xs
    return $ (Decl t xs', env')

  CondElse e s1 s2 -> do 
    (expr, t) <- inferExp env e

    (stmt1, env1@Env{retType = r1}) <- checkStm env s1
    (stmt2, env2@Env{retType = r2}) <- checkStm env s2

    let stmt = CondElse expr stmt1 stmt2

    unless (t `elem` [Bool]) $ fail $
          "(CondElse) expected bool type, but found " ++ printTree t ++
          " when checking " ++ printTree s

    case e of
      ELitTrue -> do
          env <- setRetType env r1
          return (stmt, env)
      ELitFalse -> do
          env <- setRetType env r2
          return (stmt, env)
      _ -> do
          env <- setRetType env r1
          return (stmt, env)

  Cond e s1 -> do 
    (expr, t) <- inferExp env e
    
    (stmt, env'@Env{retType = r}) <- checkStm env s1
    let stmt' = Cond expr stmt

    unless (t `elem` [Bool]) $ fail $
          "(Cond) expected bool type, but found " ++ printTree t ++
          " when checking " ++ printTree s

    case e of
      ELitTrue -> do
        env'' <- setRetType env r
        return $ (stmt', env'')
      _ -> do
        return (stmt', env)

  Ret e -> do
    (expr, t) <- inferExp env e
    df <- lookupDefType env

    -- Update return type
    env' <- setRetType env t

    unless(t == df) $ fail $
          "(Return) unexpected return type " ++ printTree t ++
          " when checking " ++ printTree s
    return (Ret expr, env')

  VRet -> do
    df <- lookupDefType env

    env <- setRetType env Void

    unless(Void == df) $ fail $
          "(VReturn) unexpected return type " ++
          " when checking " ++ printTree s
    return (s, env)

  SExp e -> do 
    (expr, t) <- inferExp env e
    return (SExp expr, env)

  Incr x -> do
    env' <- isNumeric env x
    return (s, env')

  Decr x -> do
    env' <- isNumeric env x
    return (s, env')

  Ass id e -> do 
    var_type <- lookupVar env id
    (expr, exp_type) <- inferExp env e

    unless (var_type == exp_type) $ fail $
      "trying to assign incompatible types" ++
      " when checking " ++ printTree e
    return (Ass id expr, env)

  ArrAss id e1 e2 -> do 
    var_type <- lookupVar env id
    (expr1, exp_type1) <- inferExp env e1
    (expr2, exp_type2) <- inferExp env e2

    unless (var_type == Array exp_type2) $ fail $
      "trying to assign incompatible types" ++
      " when checking " ++ printTree e1

    return (ArrAss id expr1 expr2, env)

  Empty -> do
    env <- setRetType env Void
    return (s, env)

  For t id e s -> do
    (expr, expr_type) <- inferExp env e
    
    let env' = extendCxt env id t

    unless (Array t == expr_type) $ fail $
      "iterator and array are of different type" ++
      " when checking " ++ printTree e

    (stmt, env'') <- checkStm env' s

    return $ (For t id expr stmt, env)

isNumeric :: Env -> Ident -> Err Env
isNumeric env id = do 
  t <- lookupVar env id
  unless (t `elem` [Int, Doub]) $ fail $
    "expected numberic type, but found " ++ printTree t ++
    " when checking "
  return env

updateVar :: Env -> Type -> Item -> Env
updateVar env t id = case id of
  NoInit x    -> extendCxt env x t
  Init   x e  -> extendCxt env x t

updateVars :: Env -> Type -> [Item] -> Err (Env, [Item])
updateVars env t [] = return (env, [])
updateVars env t (item:items) = case item of
  NoInit id -> do 
    (env', i) <- updateVars env t items
    env'' <- declCxt env' id t
    return (env'', [NoInit id] ++ i)
  Init id e -> do
    (expr, expr_type) <- inferExp env e
    (env', is) <- updateVars env t items
    env'' <- declCxt env' id t

    unless (t == expr_type) $ fail $ 
      show e ++ 
      "Trying to assign" ++ printTree expr_type ++ " to " ++ printTree t
    return (env'', [Init id expr] ++ is)

checkExp :: Env -> Expr -> Type -> Err (Expr)
checkExp env e t = do
  (expr, t') <- inferExp env e
  unless (t == t') $ fail $
    "expected type " ++ printTree t ++
    " but found type " ++ printTree t' ++
    " when checking expression " ++ printTree e
  return expr

inferExp :: Env -> Expr -> Err (Expr, Type)
inferExp env e = case e of

  ELitTrue  -> return (EType Bool e, Bool)
  ELitFalse -> return (EType Bool e, Bool)

  ELitInt i   -> return (EType Int e, Int)
  ELitDoub d  -> return (EType Doub e, Doub)

  EVar x    -> do 
    t <- lookupVar env x
    return (EType t e, t)

  EString s -> return (EType String e, String)

  EApp f es -> do
    FunType t ts <- lookupDef env f
    unless (length es == length ts) $ fail $
      "incorrect number of arguments to function " ++ printTree f


    es' <- zipWithM (checkExp env) es ts

    return (EType t (EApp f es'), t)

  EAdd e1 op e2 -> do
    (expr1, t1) <- inferExp env e1
    (expr2, t2) <- inferExp env e2

    unless( t1 == t2 ) $ fail $
      "(EAdd) expected numberic type, but found " ++ printTree t2 ++
      " when checking " ++ printTree e1

    return (EType t1 (EAdd expr1 op expr2), t1)

  Neg expr -> do
    (expr', t) <- inferExp env expr

    unless(t `elem` [Int, Doub] ) $ fail $
      "(Neg) expected numberic type, but found " ++ printTree t ++
      " when checking " ++ printTree expr

    return (EType t (Neg expr'), t)

  EMul e1 op e2 -> do
    (expr1, t1) <- inferExp env e1
    (expr2, t2) <- inferExp env e2

    unless(t1 == t2 && op /= Mod || op == Mod && t1 == Int && t2 == Int) $ fail $
      "(EMul) expected numberic type, but found " ++ printTree t2 ++
      " when checking " ++ printTree e1
    return (EType t1 (EMul expr1 op expr2), t1)

  ERel e1 op e2 -> do
    (expr1, t1) <- inferExp env e1
    (expr2, t2) <- inferExp env e2

    unless(t1 == t2) $ fail $
      "(EGt) expected numberic type, but found " ++ printTree t2 ++
      " when checking " ++ printTree e1
    return (EType Bool (ERel expr1 op expr2), Bool)

  EAnd e1 e2 -> do
    (expr1, t1) <- inferExp env e1
    (expr2, t2) <- inferExp env e2

    unless((t1 `elem` [Bool]) && (t2 `elem` [Bool])) $ fail $
      "(EAnd) expected numberic type, but found " ++ printTree t2 ++
      " when checking " ++ printTree e1
    return (EType Bool (EAnd expr1 expr2), Bool)

  Not e1 -> do
    (expr1, t1) <- inferExp env e1

    unless((t1 `elem` [Bool])) $ fail $
      "(EAnd) expected bool type, but found " ++ printTree t1 ++
      " when checking " ++ printTree e1

    return (EType Bool (Not expr1), Bool)

  EOr e1 e2 -> do
    (expr1, t1) <- inferExp env e1
    (expr2, t2) <- inferExp env e2

    unless((t1 `elem` [Bool]) && (t2 `elem` [Bool])) $ fail $
      "(EOr) expected numberic type, but found " ++ printTree t2 ++
      " when checking " ++ printTree e1
    return (EType Bool (EOr expr1 expr2), Bool)

  EArr i e -> do
    (Array t) <- lookupVar env i
    (expr, _t) <- inferExp env e

    return (EType t (EArr i expr), t)

  ENew t e -> do
    (expr, _t) <- inferExp env e

    unless(_t == Int) $ fail $ "Trying to index array with non-integer"

    return (EType (Array t) (ENew t expr), Array t)

  ELen id -> do
    t <- lookupVar env id
    -- Length allways returns an integer value

    unless(t `elem` [Array Int, Array Doub, Array Bool]) $ fail $ "Trying to get length of non-array object"
    return (EType Int (ELen id), Int)

lookupDef :: Env -> Ident -> Err FunType
lookupDef env f = case Map.lookup f (envSig env) of
  Nothing -> fail $ "undefined function " ++ printTree f
  Just t  -> return t

lookupVar :: Env -> Ident -> Err Type
lookupVar env x = case catMaybes $ map (Map.lookup x) (envCxt env) of
  []      -> fail $ "unbound variable " ++ printTree x
  (t : _) -> return t

extendSig :: Env -> TopDef -> Err Env
extendSig env@Env{ envSig = sig } (FnDef t f args _ss) = do
  if Map.member f sig then fail $ "duplicate definition of function " ++ printTree f else return env { envSig = Map.insert f ft (envSig env) }
  where ft = FunType t $ map (\ (Arg t _x) -> t) args

setDefType :: Env -> Type -> Err Env
setDefType env@Env{ defType = dt } t  = do
  return env { defType = t }

setRetType :: Env -> Type -> Err Env
setRetType env@Env{ retType = dt } t  = do
  return env { retType = t }

lookupDefType :: Env -> Err Type
lookupDefType env@Env{defType = dt} = do 
  return dt

declCxt :: Env -> Ident -> Type -> Err Env
declCxt env@Env{ envCxt = b : bs } x t = case lookupVar ( env { envCxt = [b] } ) x of
  Bad _ -> return env { envCxt = Map.insert x t b : bs }
  Ok _ -> fail $ "multiple declarations of variable"

-- | Add new variable with type.
extendCxt :: Env -> Ident -> Type -> Env
extendCxt env@Env{ envCxt = b : bs } x t = env { envCxt = Map.insert x t b : bs }

newBlock :: Env -> Env
newBlock env = env { envCxt = Map.empty : envCxt env, retType = Void }

exitBlock :: Env -> Env
exitBlock env@Env { envCxt = b : bs } = env { envCxt = bs }

emptyEnv :: Env
emptyEnv = Env { envSig = Map.empty, envCxt = [Map.empty], defType = Void, retType = Void }
