module CodeGen where

import Control.Monad.State

import Data.Map (Map)
import Data.List
import Data.Functor.Identity
import qualified Data.Map as Map
import Data.Maybe
import Javalette.ErrM

import Javalette.Abs
import Javalette.Lex
import Javalette.Par
import Javalette.Print

import qualified LLVM

-- * Data types and monad

-- | Environment (handled by state monad)
data Env = Env
  { contexts      :: Contexts           -- Variables
  , code          :: [LLVM.Instruction] -- The generated code
  , numLabels     :: Int                -- Number of labels
  , nextTmp       :: Int                -- Number of temp vars
  , strings       :: [LLVM.Instruction] -- Keep track of global string constants
  , varCount      :: Map Ident Int      -- Maintain count of variables with same name
  , heap          :: Map Ident Int      -- Set the allocated space for arrays
  }

type Contexts = [VarContext]

-- | Variables in scope (symbol table)
data VarContext = VarContext
  { vars  :: Map Ident LLVM.Address     -- Variable address
  , vtype :: Map Ident Type             -- Set type of variable
  , ptype :: Map Ident Bool             -- Set variable type to pointer (true) or value (false)
  , next  :: LLVM.Address               -- Next free address pointer
  , isArg :: Map Ident Bool             -- Indicates that the variable is an argument
  }

type CodeGen = State Env

-- * Service functions
emit :: LLVM.Instruction -> CodeGen ()
emit i = modify $ updateCode $ (i :)

comment :: String -> CodeGen ()
comment = emit . LLVM.Comment

raw :: String -> CodeGen ()
raw = emit . LLVM.Raw

label :: String -> CodeGen ()
label s = do
  emit . LLVM.Branch $ s
  emit . LLVM.Raw $ s ++ ":"

blank :: CodeGen ()
blank = raw ""

newBlock :: CodeGen ()
newBlock = do
  env <- gets contexts

  modify $ updateContexts $ (emptyContext env :)

exitBlock :: CodeGen ()
exitBlock = modify $ updateContexts $ tail

allocate :: Ident -> Int -> CodeGen ()
allocate x size = do
  env@Env{ heap = h } <- get
  put env{ heap = Map.insert x size h  }

getAllocated :: Ident -> CodeGen (Int)
getAllocated x = do
  h <- gets heap
  case Map.lookup x h of
    Nothing   -> return 0
    Just a    -> return a

updateVarCount :: Ident -> CodeGen ()
updateVarCount x = do
  env@Env{ varCount = vs } <- get
  cnt <- getVarCount x
  put env{ varCount = Map.insert x (cnt + 1) vs  }

getVarCount :: Ident -> CodeGen Int
getVarCount x = do
  bs <- gets varCount
  case Map.lookup x bs of
    Nothing   -> return 0
    Just a    -> return a

extendContext :: Ident -> Type -> Bool -> CodeGen ()
extendContext (Ident x) t p = do
    updateVarCount (Ident x)

    modify $ updateContexts $ \ (b : bs) ->
      b { vars = Map.insert (Ident x) (next b) (vars b)
      ,   ptype = Map.insert (Ident x) p (ptype b)
      ,   vtype = Map.insert (Ident x) t (vtype b)
      ,   next = next b + 1
      ,   isArg = Map.insert (Ident x) False (isArg b)
      } : bs

extendContextArg :: Ident -> Type -> Bool -> CodeGen ()
extendContextArg (Ident x) t p = do
    updateVarCount (Ident x)

    modify $ updateContexts $ \ (b : bs) ->
      b { vars = Map.insert (Ident x) (next b) (vars b)
      ,   ptype = Map.insert (Ident x) p (ptype b)
      ,   vtype = Map.insert (Ident x) t (vtype b)
      ,   next = next b + 1
      ,   isArg = Map.insert (Ident x) True (isArg b)
      } : bs
 
lookupVarArg :: Ident -> CodeGen Bool
lookupVarArg x = do
  bs <- gets contexts
  case catMaybes $ map (Map.lookup x . isArg) bs of
    []      -> return $ False
    (a : _) -> return a

checkExtendContext :: Ident -> Type -> Bool -> Int -> CodeGen (Ident)
checkExtendContext (Ident x) t p ix = do
  a <- lookupVar (Ident x)
  cnt <- getVarCount (Ident x)
  updateVarCount (Ident x)

  case cnt of 
    0 -> do
      extendContext (Ident x) t p
      return $ Ident x
    _ -> do
      extendContext (Ident (x ++ show (cnt))) t p

      return $ Ident (x ++ show (cnt))

-- Get a unique instance of a variable
getVar :: Ident -> CodeGen (Ident)
getVar (Ident x) = do

  cnt <- getVarCount (Ident x)

  var <- getVarInstance (Ident x) (cnt + 5)

  return $ var

-- Given a variable find a unique name by adding prefixes
getVarInstance :: Ident -> Int -> CodeGen (Ident)
getVarInstance (Ident x) ix = do
  -- If ix = 0 all other instances have been checked,
  -- return identifier without index
  if ix == 0 then
    return (Ident x)
  else do
      -- Check if the variable is defined, if it is, return
      -- the variable with the index, otherwise try again
      a <- lookupVar $ Ident (x ++ show ix)
      case a of
        -1 -> do
          var <- getVarInstance (Ident x) (ix-1)
          return $ var
        _ -> return $ Ident (x ++ show ix)

-- Lookup variable address
lookupVar :: Ident -> CodeGen LLVM.Address
lookupVar x = do
  bs <- gets contexts
  case catMaybes $ map (Map.lookup x . vars) bs of
    []      -> return $ -1
    (a : _) -> return a

-- Check if a variable in the context is a pointer or value type
lookupVarPointerType :: Ident -> CodeGen Bool
lookupVarPointerType x = do
  bs <- gets contexts
  case catMaybes $ map (Map.lookup x . ptype) bs of
    []      -> error $ "(lookupVarPointerType) unbound var " ++ printTree x
    (t : _) -> return t

-- Lookup the type of variable
lookupVarType :: Ident -> CodeGen Type
lookupVarType x = do
  bs <- gets contexts

  case catMaybes $ map (Map.lookup x . vtype) bs of
    []      -> error $ "(lookupVarType) unbound var " ++ printTree x
    (t : _) -> return t

-- * Environment
emptyEnv :: String -> Env
emptyEnv s = Env
  { contexts      = []
  , code          = []
  , numLabels     = 0
  , nextTmp       = 0
  , strings       = []
  , varCount      = Map.empty
  , heap          = Map.empty
  }

updateContexts :: (Contexts -> Contexts) -> Env -> Env
updateContexts f env = env { contexts = f (contexts env) }

updateCode :: ([LLVM.Instruction] -> [LLVM.Instruction]) -> Env -> Env
updateCode f env = env { code = f (code env) }

-- * Contexts
emptyContext :: [VarContext] -> VarContext
emptyContext bs = case bs of 
  [] -> VarContext { vars = Map.empty, vtype = Map.empty, ptype = Map.empty, next = 0, isArg = Map.empty }
  _ -> VarContext { vars = Map.empty, vtype = Map.empty, ptype = Map.empty, next = next (last bs) + 1, isArg = Map.empty}

-- * Code generator

codeGen :: FilePath -> Program -> String
codeGen filename prg = unlines (map LLVM.showInstruction jcode)
  where
    env     = emptyEnv filename
    jcode   = reverse $ code $ compileProgram prg `execState` env

compileProgram :: Program -> CodeGen ()
compileProgram (Program defs) = do

  -- Library function type declarations
  raw "declare void @printInt(i32)"
  raw "declare void @printDouble(double)"
  raw "declare void @printString(i8*)"
  raw "declare i32 @readInt()"
  raw "declare noalias i8* @calloc(i32, i32)"
  raw "declare double @readDouble()"
  raw "%i32array = type {i32, i32* }"
  raw "%i64array = type {i32, double*}"
  raw "%i1array = type {i32, i1*}"

  -- Generate LLVM output
  mapM_ compileDef defs

  -- Print global constants (strings)
  printGobals

compileDef :: TopDef -> CodeGen ()
compileDef (FnDef t f@(Ident name) args (Block ss)) = do

  let argTypes =  intercalate ", " $ map (\ (Arg t (Ident x)) -> argAlias t ++ " %__p__" ++ x) args
  blank

  -- Function definition
  raw $ "define " ++ argAlias t ++ " @" ++ name ++ "(" ++ argTypes ++ ") {"
  raw $ "entry:"

  newBlock

  mapM_ compileArgs args
  mapM_ compileStm ss

  -- Dummy return statement (never reached but required by LLVM).
  case t of
    Int -> emit $ LLVM.Return (llvmType t) (LLVM.VInt 0)
    Doub -> emit $ LLVM.Return (llvmType t) (LLVM.VDoub 0)
    Bool -> emit $ LLVM.Return (llvmType t) (LLVM.VBool False)
    Array Int -> blank
    Array Doub -> blank
    Array Bool -> blank
    Void -> emit $ LLVM.VReturn

  exitBlock

  raw $ "}"
  blank

compileArgs :: Arg -> CodeGen ()
compileArgs (Arg t (Ident x)) = do
  -- Add the variable to the current context and set isarg to true
  extendContextArg (Ident x) t True

  -- If the variable type is an array type get the pointer to that array,
  -- otherwise allocate and store the parameter value.
  var_type <- lookupVarType (Ident x)
  case var_type of
    Array t -> do
      size <- getAllocated (Ident x)

      let instr = LLVM.GetElemPtr2 (LLVM.Ident ("__p__" ++ x)) (llvmType (Array t)) (LLVM.VInt 0)

      emit $ LLVM.Alloc (LLVM.Array (llvmType t)) (LLVM.Ident x)
      emit $ LLVM.Store (LLVM.Array (llvmType t)) (LLVM.Ident x) (LLVM.Ident ("__p__" ++ x))

    _  -> do
      emit $ LLVM.Alloc (llvmType t) (LLVM.Ident x)
      emit $ LLVM.Store (llvmType t) (LLVM.Ident x) (LLVM.Ident ("__p__" ++ x))

-- Set the next prefix for temp variables
updateNextTmp :: Int -> CodeGen ()
updateNextTmp i = do
  env@Env{ nextTmp = d } <- get

  put env{ nextTmp = d + i }

-- Create and return name of a unique temporary variable
createTmp :: Type -> Bool -> CodeGen (LLVM.Op)
createTmp t p = do
  env <- get
  next <- gets nextTmp
  
  -- Create temporary variable
  let tmp = LLVM.Ident $ "t" ++ show next
  extendContext (Ident $ "t" ++ show next) t p

  -- Update temp next variable prefix
  updateNextTmp 1

  return $ tmp

-- Get the type of an expression
exprType :: Expr -> Type
exprType (EType t _) = t

-- Get the array type of an expression
arrayType :: Expr -> Type
arrayType (EType (Array t) _) = t

compileStm :: Stmt -> CodeGen ()
compileStm s = do
  emit $ LLVM.Comment (show s)
  blank
  case s of

    Cond e s -> do
      
      expr <- compileExp e

      -- Create label for true/false cond
      lab0 <- createCondLabel True
      lab1 <- createCondLabel False

      (LLVM.Ident tmpVar) <- assignTmp expr e
      vartype <- lookupVarPointerType (Ident tmpVar)
      (LLVM.Ident cvar) <- loadVar (LLVM.Ident tmpVar) expr e

      emit $ LLVM.CBranch (LLVM.Ident cvar) (LLVM.Ident lab0) (LLVM.Ident lab1)

      newBlock

      label lab0
      compileStm s
      label lab1

      exitBlock

    CondElse e s1 s2 -> do

      expr <- compileExp e

      -- Create label for true/false cond
      cond_true   <- createCondLabel True
      cond_false  <- createCondLabel False
      end_loop    <- createLabel

      (LLVM.Ident tmpVar) <- assignTmp expr e
      vartype <- lookupVarPointerType (Ident tmpVar)
      (LLVM.Ident cvar) <- loadVar (LLVM.Ident tmpVar) expr e

      emit $ LLVM.CBranch (LLVM.Ident cvar) (LLVM.Ident cond_true) (LLVM.Ident cond_false)

      newBlock

      -- if true
      label cond_true
      compileStm s1

      emit $ LLVM.Branch end_loop

      -- if false
      label cond_false
      compileStm s2

      -- end
      label end_loop

      exitBlock
      
    Ret e -> do
      expr <- compileExp e

      -- Create tmp variable
      tmpVar <- getOperator e expr

      emit $ LLVM.Return (llvmType $ exprType e) tmpVar

    BStmt (Block ss) -> do
     
      newBlock

      lab0 <- createLabel 
      label lab0
      mapM compileStm ss

      exitBlock
      blank

    While e s -> do
      comment "While loop"
      wbegin <- createLabel
      wloop <- createLabel
      wend <- createLabel

      label wbegin
      -- Evaluate expression
      expr <- compileExp e

      newBlock

      (LLVM.Ident tmpVar) <- assignTmp expr e
      vartype <- lookupVarPointerType (Ident tmpVar)
      (LLVM.Ident cvar) <- loadVar (LLVM.Ident tmpVar) expr e

      -- If true goto loop, otherwise end
      emit $ LLVM.CBranch (LLVM.Ident cvar) (LLVM.Ident wloop) (LLVM.Ident wend)
      label wloop

      compileStm s

      -- Goto condition (begin)
      emit $ LLVM.Branch wbegin
      label wend

      exitBlock

    Decl t xs -> do
      updateDecls t xs

    VRet -> do
      emit $ LLVM.VReturn

    SExp e -> do
      instr <- compileExp e

      case instr of
        LLVM.LInt i -> raw ""
        LLVM.LDoub d -> raw ""
        LLVM.Str s -> raw ""
        LLVM.LBool b -> raw ""
        LLVM.Var x -> raw ""
        _ ->
          emit $ instr

    Incr (Ident x) -> do
      (Ident var) <- getVar (Ident x)

      t <- lookupVarType (Ident var)
      
      tmpVar1 <- createTmp t False
      tmpVar2 <- createTmp t False

      emit $ LLVM.Assign tmpVar1 (LLVM.Load (llvmType t) (LLVM.Ident var))
      emit $ LLVM.Assign tmpVar2 (LLVM.Add LLVM.Int (LLVM.VInt 1) tmpVar1)
      emit $ LLVM.Store (llvmType t) (LLVM.Ident var) tmpVar2

    Decr (Ident x) -> do
      (Ident var) <- getVar (Ident x)

      t <- lookupVarType (Ident var)
      tmpVar1 <- createTmp t False
      tmpVar2 <- createTmp t False


      emit $  LLVM.Assign tmpVar1 (LLVM.Load (llvmType t) (LLVM.Ident var))
      emit $ LLVM.Assign tmpVar2 (LLVM.Add LLVM.Int (LLVM.VInt (-1)) tmpVar1)
      emit $ LLVM.Store (llvmType t) (LLVM.Ident var) tmpVar2

    -- For = for (Type Ident : Expr ) Stmt ;
    For t (Ident id) e s -> do
      comment "Foreach loop"

      let t = arrayType e

      fbegin  <- createLabel
      floop   <- createLabel
      fend    <- createLabel

      newBlock

      -- Create loop variable
      (Ident var) <- checkExtendContext (Ident id) t False 0
      emit $ LLVM.Alloc (llvmType t) (LLVM.Ident var)

      -- Create pointer index
      pointer_index <- createTmp t True
      emit $ LLVM.Alloc (llvmType t) pointer_index
      emit $ LLVM.Store (llvmType t) pointer_index (LLVM.VInt 0)

      label fbegin

      -- Compile expression and store in tmp variable
      expr <- compileExp e

      case expr of
        LLVM.Var (LLVM.Ident x) -> do
          -- Get size of array
          size <- getAllocated (Ident x)

          tmp_size <- createTmp Int False
          (LLVM.Ident var_size) <- getArraySize (LLVM.Ident x)
          assign tmp_size (LLVM.Load LLVM.Int (LLVM.Ident var_size))

          -- Create tmp variable containing element pointer
          element_pointer <- createTmp (exprType e) False
          tmp_index <- createTmp (exprType e) False
          emit $ LLVM.Assign tmp_index (LLVM.Load (llvmType t) pointer_index)

          element_pointer <- getArrayElemPtr t (Ident x) tmp_index

          -- store the element that the pointer points at into loop variable
          element_value <- createTmp (exprType e) False
          tmp_var <- createTmp (exprType e) False
          emit $ LLVM.Assign tmp_var (LLVM.Load (llvmType t) (LLVM.Ident var))
          emit $ LLVM.Assign element_value (LLVM.Load (llvmType t) element_pointer)
          emit $ LLVM.Store (llvmType t) (LLVM.Ident var) element_value

          -- Check if loop variable is less than array size
          compare <- createTmp (exprType e) False
          emit $ LLVM.Assign compare (LLVM.Gt (llvmType t) tmp_size tmp_index)

          -- If true goto loop, otherwise end
          emit $ LLVM.CBranch compare (LLVM.Ident floop) (LLVM.Ident fend)

          label floop

          compileStm s

          -- increase the pointer index
          increase <- createTmp (exprType e) False
          emit $ LLVM.Assign increase (LLVM.Add (llvmType t) tmp_index (LLVM.VInt 1))
          emit $ LLVM.Store (llvmType t) pointer_index increase

          -- Goto condition (begin)
          emit $ LLVM.Branch fbegin

          label fend

          exitBlock

        _ -> error $ "variable is not an array"

    Ass (Ident x) e -> do

      (Ident var) <- getVar (Ident x)

      t <- lookupVarType (Ident var)

      case t of 
        Array _t -> do 
          copyArrayRef e t (Ident var) 
        _ -> do
          expr <- compileExp e

          (LLVM.Ident tmpVar) <- assignTmp expr e
          vartype <- lookupVarPointerType (Ident tmpVar)
          (LLVM.Ident cvar) <- loadVar (LLVM.Ident tmpVar) expr e

          emit $ LLVM.Store (llvmType (exprType e)) (LLVM.Ident var)  (LLVM.Ident cvar)

    ArrAss (Ident x) e1 e2 -> do

      (Ident var) <- getVar (Ident x)

      expr1 <- compileExp e1
      expr2 <- compileExp e2

      (Array t) <- lookupVarType (Ident var) 

      op <- getOperator e1 expr1
      -- Get pointer to array element
      tmp3 <- getArrayPtr (Ident var) t op

      -- Store value to array
      vVar <- getOperator e2 expr2

      emit $ LLVM.Store (llvmType t) tmp3 vVar

    Empty -> do
      emit $ LLVM.Raw "%nop = add i1 0, 0"

copyArrayRef :: Expr -> Type -> Ident -> CodeGen ()
copyArrayRef e t (Ident var) = do
  expr@(LLVM.Var id) <- compileExp e

  let ptr_old = LLVM.GetElemPtr2 (LLVM.Ident var) (llvmType t) (LLVM.VInt 0)
  let ptr_new = LLVM.GetElemPtr2 id (llvmType t) (LLVM.VInt 0)

  tmp1 <- createTmp t True
  tmp2 <- createTmp t True
  tmp3 <- createTmp t True

  assign tmp1 ptr_new
  assign tmp2 ptr_old

  let load_ptr = LLVM.Load (llvmType t) id
  assign tmp3 load_ptr

  emit $ LLVM.Store (llvmType t) (LLVM.Ident var) tmp3

updateNumLabels :: Int -> CodeGen ()
updateNumLabels i = do
  env@Env{ numLabels = d } <- get

  put env{ numLabels = d + i }

getNumLabels :: CodeGen Int
getNumLabels = do
    env <- get
    d <- gets numLabels
    return d

-- Create and initialize list of variables
updateDecls :: Type -> [Item] -> CodeGen()
updateDecls t []      = do blank
updateDecls t (x:xs)  = do
      updateDecl t x
      updateDecls t xs

-- Creates and initializes variables
updateDecl :: Type -> Item -> CodeGen ()
updateDecl t item = case item of
  NoInit (Ident x) -> do
    -- Allocate space for new variable
    (Ident var) <- checkExtendContext (Ident x) t True 0
    emit $ LLVM.Alloc (llvmType t) (LLVM.Ident var)

    case t of 
      Int   -> emit $ LLVM.Store (llvmType t) (LLVM.Ident var) (LLVM.VInt 0)
      Doub  -> emit $ LLVM.Store (llvmType t) (LLVM.Ident var) (LLVM.VDoub 0)
      Bool  -> emit $ LLVM.Store (llvmType t) (LLVM.Ident var) (LLVM.VBool False)

  Init (Ident x) e -> do

    case exprType e of
      Array t -> do
        createInitArray t item
      _ -> do
        createInitVar t item

createInitVar :: Type -> Item -> CodeGen ()
createInitVar t item@(Init (Ident x) e) = do
  -- Compile assignment expression
  expr <- compileExp e

  -- Allocate space for the new variable
  (Ident var) <- checkExtendContext (Ident x) t False 0

  -- Assign expression to variable
  v <- assignVar (LLVM.Ident var) expr e
  blank


createInitArray :: Type -> Item -> CodeGen ()
createInitArray t item@(Init (Ident x) e) = do

  expr <- compileExp e

  -- If the declaration is an array declaration we have to deal
  -- with three cases
  -- 1) it is a function returning an array
  -- 2) it is a new constructor
  -- 3) it is an array being assigned 
  case e of
    (EType _t (EApp f es)) -> do
      -- Allocate space for the new variable
      (Ident var) <- checkExtendContext (Ident x) (Array t) True 0

      emit $ LLVM.Alloc (llvmType (Array t)) (LLVM.Ident var) 

      -- Store call into tmp variable
      tmp_expr <- createTmp (exprType e) False
      assign tmp_expr expr

      -- Store tmp variable into declaration variable
      emit $ LLVM.Store (llvmType (Array t)) (LLVM.Ident var) tmp_expr

    (EType _t (ENew __t _e)) -> do
      -- Allocate space for the new variable
      (Ident var) <- checkExtendContext (Ident x) (Array t) True 0
      assign (LLVM.Ident var) (LLVM.LArray (llvmType t))

      -- Set array size
      op <- getInstrVar expr

      case op of
        LLVM.VInt i -> do
          setArraySize (LLVM.Ident var) op
          -- Allocate array elements
          arrayAlloc (Ident var) __t op
        LLVM.Ident x -> do
          tmpVar <- createTmp (exprType e) False
          emit $ LLVM.Assign tmpVar (LLVM.Load LLVM.Int (LLVM.Ident x))

          setArraySize (LLVM.Ident var) tmpVar

          arrayAlloc (Ident var) t tmpVar

    _ -> do
      -- Allocate space for the new variable
      (Ident var) <- checkExtendContext (Ident x) (Array t) True 0

      assign (LLVM.Ident var) (LLVM.LArray (llvmType t))

      copyArrayRef e (Array t) (Ident var)

getInstrVar :: LLVM.Instruction -> CodeGen LLVM.Op
getInstrVar instr = case instr of
  LLVM.Var x -> return x
  LLVM.LInt x -> return $ LLVM.VInt x
  _ -> error $ show instr

arrayAlloc :: Ident -> Type -> LLVM.Op -> CodeGen ()
arrayAlloc (Ident x) t op = do
  (LLVM.Ident tmp1) <- createTmp t False
  (LLVM.Ident tmp2) <- createTmp t False
  (LLVM.Ident tmp3) <- createTmp t False
  (LLVM.Ident tmp4) <- createTmp t False
  (LLVM.Ident tmp5) <- createTmp t False

  -- Initialize array
  raw $ "%" ++ tmp1 ++ " = getelementptr " ++ typeAlias t ++ "* null, i32 1"
  raw $ "%" ++ tmp2 ++ " = ptrtoint " ++ typeAlias t ++ "* %" ++ tmp1 ++ " to i32"
  raw $ "%" ++ tmp3 ++ " = call i8* @calloc(i32 " ++ showOp op ++", i32 %" ++ tmp2 ++ ")"
  raw $ "%" ++ tmp4 ++ " = bitcast i8* %" ++ tmp3 ++ " to " ++ typeAlias t ++ "*"

  raw $ "%" ++ tmp5 ++ " = getelementptr " ++ typeAlias (Array t) ++ "* %" ++ x ++ ", i32 0, i32 1"
  raw $ "store " ++ typeAlias t ++ "* %" ++ tmp4 ++ ", " ++ typeAlias t ++ "** %" ++ tmp5

assign :: LLVM.Op -> LLVM.Instruction -> CodeGen ()
assign op instr = emit $ LLVM.Assign op instr

loadVar :: LLVM.Op -> LLVM.Instruction -> Expr -> CodeGen (LLVM.Op)
loadVar (LLVM.Ident var) expr e = do
  varTyp <- lookupVarPointerType (Ident var)
  case varTyp of 
    True -> do
      case expr of
        LLVM.LInt i -> do
          tmpVar <- createTmp Int False
          emit $ LLVM.Assign tmpVar (LLVM.Load LLVM.Int (LLVM.Ident var))
          return tmpVar
        LLVM.LDoub d -> do
          tmpVar <- createTmp Doub False
          emit $ LLVM.Assign tmpVar (LLVM.Load LLVM.Doub (LLVM.Ident var))
          return tmpVar
        LLVM.LBool b -> do
          tmpVar <- createTmp Bool False
          emit $ LLVM.Assign tmpVar (LLVM.Load LLVM.Bool (LLVM.Ident var))
          return tmpVar
        LLVM.Var x -> do
          tmpVar <- createTmp (exprType e) False
          emit $ LLVM.Assign tmpVar (LLVM.Load (llvmType (exprType e)) (LLVM.Ident var))
          return tmpVar
        _ -> do return (LLVM.Ident var)
    False -> do return (LLVM.Ident var)

assignVar :: LLVM.Op -> LLVM.Instruction -> Expr -> CodeGen (LLVM.Op)
assignVar var expr e@(EType t _e) = case expr of 
  LLVM.LInt i -> do
    emit $ LLVM.Alloc (llvmType (exprType e)) var
    emit $ LLVM.Store LLVM.Int var (LLVM.VInt i)
    return var
  LLVM.LDoub d -> do
    emit $ LLVM.Alloc (llvmType (exprType e)) var
    emit $ LLVM.Store LLVM.Doub var (LLVM.VDoub d)
    return var
  LLVM.LBool b -> do
    emit $ LLVM.Alloc (llvmType (exprType e)) var
    emit $ LLVM.Store LLVM.Bool var (LLVM.VBool b)
    return var
  LLVM.LStr s -> do
    emit $ LLVM.Alloc (llvmType (exprType e)) var
    emit $ LLVM.Store LLVM.String var (LLVM.VStr s)
    return var
  LLVM.LArray t -> do
    emit $ LLVM.Assign var (LLVM.LArray t)
    return var
  LLVM.Var x -> do
    emit $ LLVM.Alloc (llvmType (exprType e)) var
    let t = llvmType ( exprType e )

    tmp <- createTmp ( exprType e ) False
    emit $ LLVM.Assign tmp (LLVM.Load t x)

    emit $ LLVM.Store t var tmp
    return tmp
  _ -> do

    case t of
      Array t -> do
        tmp <- createTmp (exprType e) True
        emit $ LLVM.Assign tmp expr
        return tmp
      _ -> do
        emit $ LLVM.Alloc (llvmType (exprType e)) var
        tmp <- createTmp (exprType e) True
        emit $ LLVM.Assign tmp expr
        emit $ LLVM.Store (llvmType (exprType e)) var tmp
        return tmp

assignTmp ::  LLVM.Instruction -> Expr -> CodeGen (LLVM.Op)
assignTmp expr e = case expr of 
  LLVM.LInt i -> do
    tmpVar <- createTmp (exprType e) True
    emit $ LLVM.Alloc LLVM.Int tmpVar
    emit $ LLVM.Store LLVM.Int tmpVar (LLVM.VInt i)
    return tmpVar
  LLVM.LDoub d -> do
    tmpVar <- createTmp (exprType e) True
    emit $ LLVM.Alloc LLVM.Doub tmpVar
    emit $ LLVM.Store LLVM.Doub tmpVar (LLVM.VDoub d)
    return tmpVar
  LLVM.LBool b -> do
    tmpVar <- createTmp (exprType e) True
    emit $ LLVM.Alloc LLVM.Bool tmpVar
    emit $ LLVM.Store LLVM.Bool tmpVar (LLVM.VBool b)
    return tmpVar
  LLVM.Var (LLVM.Ident x) -> do
    let t = llvmType ( exprType e )
    tmpVar <- createTmp (exprType e) False

    (Ident var) <- getVar (Ident x)

    var_type <- lookupVarType (Ident x)
    case var_type of
      Array t -> do
        tmpVar <- getArrayPtr (Ident x) (Array t) (LLVM.VInt 0)
        return tmpVar
      _ -> do 
        emit $ LLVM.Assign tmpVar (LLVM.Load t (LLVM.Ident x))
        return tmpVar
  LLVM.LArray t -> do
    let t = llvmType ( exprType e )
    tmpVar <- createTmp (exprType e) False
    emit $ LLVM.Assign tmpVar (LLVM.LArray t)
    return tmpVar
  _ -> do
    tmpVar <- createTmp (exprType e) False
    emit $ LLVM.Assign tmpVar expr
    return tmpVar



-- Type translations, should be compatible
jltype :: LLVM.Type -> Type
jltype t = case t of
  LLVM.Int               -> Int           
  LLVM.Doub              -> Doub          
  LLVM.Bool              -> Bool              
  LLVM.Void              -> Void            
  LLVM.String            -> String        
  LLVM.Array LLVM.Int    -> Array Int     
  LLVM.Array LLVM.Doub   -> Array Doub    
  LLVM.Array LLVM.Bool   -> Array Bool    
  LLVM.Array LLVM.String -> Array String  

-- Type translations, should be compatible
llvmType :: Type -> LLVM.Type
llvmType t = case t of
  Int           -> LLVM.Int
  Doub          -> LLVM.Doub
  Bool          -> LLVM.Bool
  Void          -> LLVM.Void
  String        -> LLVM.String
  Array Int     -> LLVM.Array LLVM.Int
  Array Doub    -> LLVM.Array LLVM.Doub
  Array Bool    -> LLVM.Array LLVM.Bool
  Array String  -> LLVM.Array LLVM.String

-- Create a unique llvm label
createLabel :: CodeGen String
createLabel = do
  i <- getNumLabels
  updateNumLabels 1
  return $ "lab" ++ show i

-- Create a unique llvm label indicating it is
-- used for a if/else statement
createCondLabel :: Bool -> CodeGen String
createCondLabel b = case b of
  True -> do
    i <- getNumLabels
    updateNumLabels 1
    return $ "cond_true_" ++ show i
  False -> do
    i <- getNumLabels
    updateNumLabels 1
    return $ "cond_false_" ++ show i

-- Unused function, to be removed
branchLabel :: Int -> String -> String
branchLabel i s = s ++ "_" ++ show i

-- Append a string to the list of global variables 
appendString :: String -> CodeGen (String, Int)
appendString s = do
  env@Env{ strings = ss } <- get

  let x     = ".str" ++ show (length ss)
  let instr = LLVM.Assign (LLVM.Global x) (LLVM.LStr s)

  extendContext (Ident x) String True

  put env{ strings = instr : ss }

  return (x, length s)

-- Print all global variables
printGobals :: CodeGen ()
printGobals = do
  env@Env{ strings = ss } <- get

  mapM_ printGobal ss

  blank

printGobal :: LLVM.Instruction -> CodeGen ()
printGobal instr = do
  emit $ instr

compileExp :: Expr -> CodeGen (LLVM.Instruction)
compileExp (EType t e) = do
  emit $ LLVM.Comment (show e)
  case e of 
    ELitTrue            -> return $ LLVM.LBool True
    ELitFalse           -> return $ LLVM.LBool False
    ELitInt i           -> return $ LLVM.LInt i
    ELitDoub d          -> return $ LLVM.LDoub d
    EVar (Ident x)      -> do
      (Ident var) <- getVar (Ident x)
      return $ LLVM.Var $ LLVM.Ident var
    EString s           -> do
      (x, len) <- appendString s

      return $ LLVM.GetElemPtr4 (LLVM.Global x) LLVM.Char (len + 1) (LLVM.VInt 0)

    EApp (Ident f) es   -> do
      args <- createArgs es
      return $ LLVM.Call (llvmType t) f args

    EAdd e1 op e2 -> do
      instr <- compileExprs (addOperator op) e1 e2
      return $ instr

    EMul e1 op e2 -> do
      instr <- compileExprs (mulOperator op) e1 e2
      return $ instr

    Neg e -> do
      expr <- compileExp e

      tmp <- assignTmp expr e
      cvar <- loadVar tmp expr e
      case t of
        Int -> do
          return $ LLVM.Mul (llvmType t) (LLVM.VInt (-1)) cvar
        Doub -> do
          return $ LLVM.Mul (llvmType t) (LLVM.VDoub (-1)) cvar  

    ERel e1 op e2 -> do
      instr <- compileExprs (relOperator op) e1 e2
      return $ instr

    EAnd e1 e2    -> do

      cond_true <- createCondLabel True
      cond_false <- createCondLabel False

      -- Assume false
      tmp_ret <- assignTmp (LLVM.LBool False) (EType Bool (ELitTrue))

      -- Compile first expression
      expr1 <- compileExp e1
      tmp1 <- assignTmp expr1 e1
      tmp1 <- loadVar tmp1 expr1 e1
      
      tmp1_and <- assignTmp (LLVM.And (llvmType (exprType e1)) tmp1 (LLVM.VBool True)) (EType Bool (EAnd e1 e2)) 

      -- If e1 -> cond_true, else -> cond_false
      emit $ LLVM.CBranch tmp1_and  (LLVM.Ident cond_true) (LLVM.Ident cond_false)
      label cond_true

      expr2 <- compileExp e2
      tmp2 <- assignTmp expr2 e2
      tmp2 <- loadVar tmp2 expr2 e2

      tmp2_and <- assignTmp (LLVM.And (llvmType (exprType e2)) tmp2 (LLVM.VBool True)) (EType Bool (EAnd e2 e2))

      emit $ LLVM.Store LLVM.Bool tmp_ret tmp2_and

      label cond_false

      return $ LLVM.Var tmp_ret

    Not e -> do
      expr  <- compileExp e
      tmp   <- assignTmp expr e
      tmp2  <- loadVar tmp expr e

      -- Not operation is performed using xor
      case t of 
        Int   -> return $ LLVM.Xor (llvmType t) tmp2 (LLVM.VInt 1)
        Doub  -> return $ LLVM.Xor (llvmType t) tmp2 (LLVM.VDoub 1)
        Bool  -> return $ LLVM.Xor (llvmType t) tmp2 (LLVM.VInt 1)
      
    EOr e1 e2     -> do
      cond_true <- createCondLabel True
      cond_false <- createCondLabel False

      -- Assume true
      tmp_ret <- assignTmp (LLVM.LBool True) (EType Bool (ELitTrue))

      -- Compile first expression
      expr1 <- compileExp e1
      tmp1 <- assignTmp expr1 e1
      tmp1 <- loadVar tmp1 expr1 e1

      tmp1_or <- assignTmp (LLVM.Or (llvmType (exprType e1)) tmp1 (LLVM.VBool False)) (EType Bool (EAnd e1 e2)) 

      -- If e1 -> cond_true, else -> cond_false
      emit $ LLVM.CBranch tmp1_or  (LLVM.Ident cond_true) (LLVM.Ident cond_false)
      label cond_false

      expr2 <- compileExp e2
      tmp2 <- assignTmp expr2 e2
      tmp2 <- loadVar tmp2 expr2 e2

      tmp2_or <- assignTmp (LLVM.Or (llvmType (exprType e2)) tmp2 (LLVM.VBool False)) (EType Bool (EAnd e2 e2))

      emit $ LLVM.Store LLVM.Bool tmp_ret tmp2_or

      label cond_true

      return $ LLVM.Var tmp_ret

    EArr i e -> do

      -- Compile expression
      expr <- compileExp e
      expr_tmp <- assignTmp expr e
      (LLVM.Ident expr_tmp) <- loadVar expr_tmp expr e

      -- Get array variable name
      (Ident var) <- getVar i

      size <- getAllocated (Ident var)

      -- Assign pointer to tmpVar
      ptr <- getArrayElemPtr t (Ident var) (LLVM.Ident expr_tmp) 

      return $ LLVM.Var ptr

    ELen x -> do
      (Ident var) <- getVar x
      lvar <- getArraySize (LLVM.Ident var)
      return $ LLVM.Var lvar

    ENew t e -> do
      expr <- compileExp e 

      return $ expr

compileExp e = case e of 
  _ -> return $ LLVM.Comment ("Non exhaustive" ++ (show e))

getArrayElemPtr :: Type -> Ident -> LLVM.Op -> CodeGen (LLVM.Op)
getArrayElemPtr t (Ident var) op = do
  (LLVM.Ident tmp1) <- createTmp t False
  (LLVM.Ident tmp2) <- createTmp t False
  (LLVM.Ident tmp3) <- createTmp t False

  raw $ "%" ++ tmp1 ++ " = getelementptr inbounds " ++ typeAlias (Array t) ++ "* %" ++ var ++ ", i32 0, i32 1"
  raw $ "%" ++ tmp2 ++ " = load " ++ typeAlias t ++ "** %" ++ tmp1
  raw $ "%" ++ tmp3 ++ " = getelementptr inbounds " ++ typeAlias t ++ "* %"++ tmp2 ++", i32 " ++ showOp op

  return $ (LLVM.Ident tmp3)

showOp :: LLVM.Op -> String
showOp op = case op of
  LLVM.VDoub d -> show d
  LLVM.VStr s  -> s
  LLVM.VInt i  -> show i 
  LLVM.VBool b -> case b of 
      True  -> show 1
      False -> show 0 
  LLVM.Ident s -> "%" ++ s
  LLVM.Global s -> "@" ++ s

getArrayPtr :: Ident -> Type -> LLVM.Op -> CodeGen LLVM.Op 
getArrayPtr (Ident x) t op = do
  -- Get pointer to array elements
  (LLVM.Ident tmp1) <- createTmp (Array t) True
  let instr = LLVM.GetElemPtr2 (LLVM.Ident x) (llvmType (Array t)) (LLVM.VInt 1)
  
  case instr of 
    LLVM.Var (LLVM.Ident x) -> do

      -- Get pointer to array element
      (LLVM.Ident tmp2) <- createTmp (Array t) True
      (LLVM.Ident tmp3) <- createTmp (Array t) True

      raw $ "%" ++ tmp2 ++ " = load " ++ typeAlias t ++ "** %" ++ x

      assign (LLVM.Ident tmp3) (LLVM.GetElemPtr3 (LLVM.Ident tmp2) (llvmType t) op)

      return (LLVM.Ident tmp3)

    _ -> do
      assign (LLVM.Ident tmp1) instr
      -- Get pointer to array element
      (LLVM.Ident tmp2) <- createTmp (Array t) True
      (LLVM.Ident tmp3) <- createTmp (Array t) True

      raw $ "%" ++ tmp2 ++ " = load " ++ typeAlias t ++ "** %" ++ tmp1

      assign (LLVM.Ident tmp3) (LLVM.GetElemPtr3 (LLVM.Ident tmp2) (llvmType t) op)

      return (LLVM.Ident tmp3)


getElemPtr :: LLVM.Op -> LLVM.Type -> LLVM.Op -> CodeGen (LLVM.Instruction)
getElemPtr (LLVM.Ident var) t ix = do
      -- Get array size
      size <- getAllocated (Ident var)

      -- Check if variable is an argument / parameter
      is_arg <- lookupVarArg (Ident var)

      -- Check if variable is a pointer
      is_ptr <- lookupVarPointerType (Ident var)

      if not is_arg && size > 0 && not is_ptr then
        return $ (LLVM.GetElemPtr (LLVM.Ident var) t size ix)
      else if size == 0 && not is_arg then do

        -- Load pointer
        t1 <- createTmp (jltype t) False
        assign t1 (LLVM.Load (LLVM.Array t) (LLVM.Ident var))

        -- Get element pointer
        t2 <- createTmp (jltype t) False
        return $ LLVM.GetElemPtr2 t1 t ix
        
      else if is_arg then do
        ret_var <- getArrayElemPtr (jltype t) (Ident var) ix

        return $ (LLVM.Var ret_var)
      else
        return $ (LLVM.GetElemPtr2 (LLVM.Ident var) t ix)

-- Helper function for compiling multiple expressions
compileExprs :: (LLVM.Type -> LLVM.Op -> LLVM.Op -> LLVM.Instruction) 
  -> Expr -> Expr -> CodeGen (LLVM.Instruction)
compileExprs instr ex1@(EType t1 e1) ex2@(EType t2 e2) = do

    expr1 <- compileExp ex1
    expr2 <- compileExp ex2

    tmp1 <- getOperator ex1 expr1
    tmp2 <- getOperator ex2 expr2

    return $ instr (llvmType t1) tmp1 tmp2 

-- Helper function to compile and return the LLVM.Op of
-- an expression
getOperator :: Expr -> LLVM.Instruction -> CodeGen LLVM.Op
getOperator expr@(EType t e) ec = case e of
  ELitTrue    -> return $ LLVM.VInt 1
  ELitFalse   -> return $ LLVM.VInt 0
  ELitInt i   -> return $ LLVM.VInt i
  ELitDoub d    -> return $ LLVM.VDoub d

  EVar (Ident x)-> do

    (Ident var) <- getVar (Ident x)
    var_type <- lookupVarType (Ident var)

    -- If the type of variable is array, return the pointer to the first element 
    -- of that array
    case var_type of
      Array t   -> do
          size <- getAllocated (Ident var)
          tmp <- createTmp (Array t) False

          (Ident var) <- getVar (Ident var)

          (LLVM.Ident cvar) <- loadVar (LLVM.Ident var) ec expr 

          return $ (LLVM.Ident cvar)
      _  -> do
          tmp <- createTmp t False

          (Ident var) <- getVar (Ident var)
          emit $ LLVM.Assign tmp (LLVM.Load (llvmType t) (LLVM.Ident var))
          return $ tmp

  EString s -> return $ LLVM.VStr s
  _ -> do
    tmp <- assignTmp ec (EType t e)

    tmp_ret <- loadVar tmp ec (EType t e)

    return $ tmp_ret

setArraySize :: LLVM.Op -> LLVM.Op -> CodeGen ()
setArraySize (LLVM.Ident x) op = do
  t <- lookupVarType (Ident x)

  tmpVar1 <- createTmp t True

  assign tmpVar1 (LLVM.GetElemPtr2 (LLVM.Ident x) (llvmType t) (LLVM.VInt 0))

  emit $ LLVM.Store (llvmType Int) tmpVar1 op

getArraySize :: LLVM.Op -> CodeGen LLVM.Op
getArraySize (LLVM.Ident x) = do
  t <- lookupVarType (Ident x)

  tmpVar1 <- createTmp (Array Int) True

  assign tmpVar1 (LLVM.GetElemPtr2 (LLVM.Ident x) (llvmType t) (LLVM.VInt 0))

  tmpVar2 <- createTmp t True
  assign tmpVar2 (LLVM.Load (llvmType Int) tmpVar1)

  tmpVar3 <- createTmp Int True
  emit $ LLVM.Alloc (llvmType Int) tmpVar3 
  emit $ LLVM.Store (llvmType Int) tmpVar3 tmpVar2

  return tmpVar3

mulOperator :: MulOp -> LLVM.Type -> LLVM.Op -> LLVM.Op -> LLVM.Instruction
mulOperator op = case op of
  Times -> LLVM.Mul
  Div   -> LLVM.Div
  Mod   -> LLVM.Mod

addOperator :: AddOp -> LLVM.Type -> LLVM.Op -> LLVM.Op -> LLVM.Instruction
addOperator op = case op of
  Plus    -> LLVM.Add
  Minus   -> LLVM.Sub

relOperator :: RelOp -> LLVM.Type -> LLVM.Op -> LLVM.Op -> LLVM.Instruction
relOperator op = case op of
  LTH   -> LLVM.Lt
  LE    -> LLVM.Le
  GTH   -> LLVM.Gt
  GE    -> LLVM.Ge
  EQU   -> LLVM.Eq
  NE    -> LLVM.Ne

createArgs :: [Expr] -> CodeGen ([LLVM.Args])
createArgs es = mapM createArg es

createArg :: Expr -> CodeGen (LLVM.Args)
createArg e = do
    comment "Create function call arguments"

    case exprType e of 
      Array t -> do
        -- Get variable name
        expr@(LLVM.Var (LLVM.Ident var)) <- compileExp e

        (LLVM.Ident cvar) <- loadVar (LLVM.Ident var) expr e     

        return $ LLVM.Arg (llvmType (exprType e)) cvar

      _  -> do
        expr <- compileExp e

        (LLVM.Ident tmpVar) <- assignTmp expr e
        (LLVM.Ident cvar) <- loadVar (LLVM.Ident tmpVar) expr e

        return $ LLVM.Arg (llvmType (exprType e)) cvar

typeSize :: Type -> Int
typeSize t = case t of 
  Int   -> 32
  Doub  -> 64
  Bool  -> 1

-- | Get the LLVM type alias for a type
typeAlias :: Type -> String
typeAlias t = case t of
  Int           -> "i32"
  Doub          -> "double"
  Bool          -> "i1"
  Void          -> "void"
  String        -> "i8*"
  Array Int     -> "%i32array"
  Array Bool    -> "%i1array"
  Array Doub    -> "%i64array"
  Array String  -> "i8**"

-- | Get the LLVM type alias for a type
argAlias :: Type -> String
argAlias t = case t of
  Int           -> "i32"
  Doub          -> "double"
  Bool          -> "i1"
  Void          -> "void"
  String        -> "i8*"
  Array Int     -> "%i32array"
  Array Bool    -> "i1*"
  Array Doub    -> "double*"
  Array String  -> "i8**"