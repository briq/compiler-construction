-- | LLVM intermediate representation
module LLVM where

import Data.List

data Type =
   Int
 | Doub
 | Bool
 | Void
 | Char
 | String
 | Array Type
 | Fun Type [Type]
  deriving (Eq,Ord,Show,Read)

type Address = Int
type Ident = String
type Global = String
type Label = String
type Procedure = String

type Size = Int

data Args = Arg Type String deriving (Show, Eq)

data VarType 
  = IntVal
  | IntPt

data Op 
  = Ident String
  | Global String
  | VInt Integer
  | VBool Bool
  | VDoub Double
  | VStr String
  deriving (Show, Eq)

data Instruction
  = Load Type Op
  | Alloc Type Op
  | Assign Op Instruction
  | Store Type Op Op
  | Branch Label
  | CBranch Op Op Op
  | And Type Op Op
  | Or Type Op Op
  | Xor Type Op Op
  | Add Type Op Op
  | Sub Type Op Op
  | Mul Type Op Op
  | Div Type Op Op
  | Mod Type Op Op
  | Call Type Ident [Args]
  | Lt Type Op Op
  | Le Type Op Op
  | Gt Type Op Op
  | Ge Type Op Op
  | Eq Type Op Op
  | Ne Type Op Op
  | Return Type Op
  | GetElemPtr Op Type Int Op
  | GetElemPtr2 Op Type Op
  | GetElemPtr3 Op Type Op
  | GetElemPtr4 Op Type Int Op
  | VReturn
  | LInt Integer
  | LDoub Double
  | LBool Bool
  | LStr String
  | LArray Type
  | Var Op
  | Str String
  | Raw String
  | Comment String
  deriving (Eq,Show)

showInstruction :: Instruction -> String
showInstruction (Load t o)        = "load " ++ typeAlias t ++ "* " ++ showOp o ++ typeAlign t
showInstruction (Alloc t x)       = showOp x ++ " = alloca " ++ typeAlias t ++ typeAlign t
showInstruction (Assign o i)      = showOp o ++ " = " ++ showInstruction i
showInstruction (Store t x op)    = "store " ++ typeAlias t ++ " " ++ showOp op ++ ", " ++ typeAlias t ++ "* " ++ (showOp x) ++ typeAlign t

showInstruction (Branch l)        = "br label %" ++ l
showInstruction (CBranch x l1 l2) = "br i1 " ++ showOp x ++ ", label " ++ showOp l1 ++ " , label " ++ showOp l2

showInstruction (Add t o2 o3)     = addFunc t ++ " " ++ typeAlias t ++ " " ++ (showOp o2) ++ ", " ++ (showOp o3)
showInstruction (Sub t o2 o3)     = subFunc t ++ " " ++ typeAlias t ++ " " ++ (showOp o2) ++ ", " ++ (showOp o3)
showInstruction (Mul t o2 o3)     = mulFunc t ++ " " ++  typeAlias t ++ " " ++ (showOp o2) ++ ", " ++ (showOp o3)
showInstruction (Div t o2 o3)     = divFunc t ++ " " ++ typeAlias t ++ " " ++ (showOp o2) ++ ", " ++ (showOp o3)
showInstruction (Mod t o2 o3)     = "srem " ++ typeAlias t ++ " " ++ (showOp o2) ++ ", " ++ (showOp o3)

showInstruction (Lt t o1 o2)      = cmpFunc t "lt" ++ " " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2
showInstruction (Le t o1 o2)      = cmpFunc t "le" ++ " " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2
showInstruction (Gt t o1 o2)      = cmpFunc t "gt" ++ " " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2
showInstruction (Ge t o1 o2)      = cmpFunc t "ge" ++ " " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2
showInstruction (Eq t o1 o2)      = cmpFunc t "eq" ++ " " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2
showInstruction (Ne t o1 o2)      = cmpFunc t "ne" ++ " " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2

showInstruction (And t o1 o2)     = "and " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2
showInstruction (Or t o1 o2)      = "or " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2
showInstruction (Xor t o1 o2)     = "xor " ++ typeAlias t ++ " " ++ showOp o1 ++ ", " ++ showOp o2

showInstruction (Comment s)       = "; " ++ (intercalate " " (words s))
showInstruction (Return t o)      = "ret " ++ typeAlias t ++ " " ++ showOp o
showInstruction (VReturn)         = "ret void"
showInstruction (Raw s)           = s

showInstruction (LInt i)          = show i
showInstruction (LDoub d)         = show d
showInstruction (Str s)           = s
showInstruction (LStr s)          = "internal constant ["++ show (length s + 1) ++" x i8] c" ++ "\"" ++ s ++ "\\" ++ "00\""
showInstruction (Var x)           = showOp x
showInstruction (LBool True)      = show 1
showInstruction (LBool False)     = show 0
showInstruction (LArray t)        = "alloca " ++ typeAlias (Array t)

showInstruction (GetElemPtr x t l ix) = "getelementptr inbounds " ++ typeAlias t ++ "* " ++ showOp x ++ ", i32 0, i32 " ++ showOp ix
showInstruction (GetElemPtr2 x t ix)  = "getelementptr inbounds " ++ typeAlias t ++ "* " ++ showOp x ++ ", i32 0, i32 " ++ showOp ix
showInstruction (GetElemPtr3 x t ix)  = "getelementptr inbounds " ++ typeAlias t ++ "* " ++ showOp x ++ ", i32 " ++ showOp ix

showInstruction (GetElemPtr4 x t l ix) = "getelementptr inbounds [" ++ show l ++ " x " ++ typeAlias t ++ "]* " ++ showOp x ++ ", i32 0, i32 " ++ showOp ix

showInstruction (Call t f args)   = "call " ++ typeAlias t ++ " @" ++ f ++ "(" ++ showArgs args ++ ")" 

typeAlign :: Type -> String
typeAlign t = case t of
  Int -> ", align 4"
  Doub -> ", align 8"
  _ -> ""


subFunc :: Type -> String
subFunc t = case t of
  Int   -> "sub"
  Doub  -> "fsub"

addFunc :: Type -> String
addFunc t = case t of
  Int   -> "add"
  Doub  -> "fadd"

mulFunc :: Type -> String
mulFunc t = case t of
  Int   -> "mul"
  Doub  -> "fmul"
 
divFunc :: Type -> String
divFunc t = case t of
  Int   -> "sdiv"
  Doub  -> "fdiv"

cmpFunc :: Type -> String -> String
cmpFunc t op = case t of
  Bool -> case op of
    "lt" -> "icmp slt"
    "le" -> "icmp sle"
    "gt" -> "icmp sgt"
    "ge" -> "icmp sge"
    "eq" -> "icmp eq"
    "ne" -> "icmp ne"
  Int -> case op of
    "lt" -> "icmp slt"
    "le" -> "icmp sle"
    "gt" -> "icmp sgt"
    "ge" -> "icmp sge"
    "eq" -> "icmp eq"
    "ne" -> "icmp ne"
  Doub -> case op of 
    "lt" -> "fcmp ult"
    "le" -> "fcmp ule"
    "gt" -> "fcmp ugt"
    "ge" -> "fcmp uge"
    "eq" -> "fcmp ueq"
    "ne" -> "fcmp une"

typeAlias :: Type -> String
typeAlias t = case t of
  Int           -> "i32"
  Doub          -> "double"
  Bool          -> "i1"
  Void          -> "void"
  Char          -> "i8"
  String        -> "i8*"
  Array Int     -> "%i32array"
  Array Bool    -> "%i1array"
  Array Doub    -> "%i64array"
  Array String  -> "i8**"

showArgs :: [Args] -> String
showArgs args = intercalate "," (map showArg args)

showArg :: Args -> String
showArg (Arg t x) = intercalate " " [typeAlias t, "%" ++ x]

showOp :: Op -> String
showOp op = case op of
  VDoub d -> show d
  VStr s  -> s
  VInt i  -> show i 
  VBool b -> case b of 
      True  -> show 1
      False -> show 0 
  Ident s -> "%" ++ s
  Global s -> "@" ++ s
