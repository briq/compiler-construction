import System.IO
import System.FilePath.Posix
import System.Environment
import System.Exit
import System.Process

import Control.Monad.Reader

import GHC.IO.Handle
import System.IO

import Javalette.Abs
import Javalette.Lex
import Javalette.Par
import Javalette.Print
import Javalette.ErrM

import TypeChecker
import CodeGen

-- driver

check :: FilePath -> String -> IO ()
check file s =
  case pProgram (myLexer s) of
    Bad err  -> do
      hPutStrLn stderr "SYNTAX ERROR"
      hPutStrLn stderr err
      exitFailure
    Ok tree -> do
      case typecheck tree of
        Bad err -> do
          print tree
          hPutStrLn stderr "TYPE ERROR"
          hPutStrLn stderr err

          exitFailure
        Ok (tree) -> do

          let name   = takeBaseName file
          let llvm   = codeGen name tree
          let dir    = takeDirectory file
          let llfile = replaceExtension file "ll"

          print tree

          writeFile llfile llvm  

          -- Assemble
          pHandle <- runProcess
                     "llvm-as" -- executable
                     [llfile] -- arguments
                     Nothing -- working directory
                     Nothing -- environment
                     Nothing -- input handle
                     (Just stderr) -- output handle
                     Nothing -- error handle
          exitCode <- waitForProcess pHandle

         -- Link
          pHandle <- runProcess
                     "llvm-link" -- executable
                     [llfile,"lib/runtime.ll","-o","main.bc"] -- arguments
                     Nothing -- working directory
                     Nothing -- environment
                     Nothing -- input handle
                     (Just stderr) -- output handle
                     Nothing -- error handle
          exitCode <- waitForProcess pHandle

          -- Compile to target architecture
          pHandle <- runProcess
                     "llc" -- executable
                     ["-filetype=obj","main.bc"] -- arguments
                     Nothing -- working directory
                     Nothing -- environment
                     Nothing -- input handle
                     (Just stderr) -- output handle
                     Nothing -- error handle
          exitCode <- waitForProcess pHandle

          -- Create executable
          pHandle <- runProcess
                     "gcc" -- executable
                     ["main.o"] -- arguments
                     Nothing -- working directory
                     Nothing -- environment
                     Nothing -- input handle
                     (Just stderr) -- output handle
                     Nothing -- error handle
          exitCode <- waitForProcess pHandle

          hPutStrLn stderr "OK"

main :: IO ()
main = do
  args <- getArgs
  case args of
    [file] -> readFile file >>= check file
    _      -> do
      putStrLn "Usage: jlc <SourceFile>"
      exitFailure
